module br.com.casadocodigo.http {
	exports br.com.casadocodigo.data;

	requires transitive br.com.casadocodigo.domain;
	requires java.net.http;
}