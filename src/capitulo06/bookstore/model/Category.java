package capitulo06.bookstore.model;

public enum Category {
	PROGRAMMING, DESIGN, AGILE, CERTIFICATION, BUSINESS
}
