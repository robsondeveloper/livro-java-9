package capitulo06.bookstore.service;

import java.util.concurrent.SubmissionPublisher;

import capitulo06.bookstore.model.Book;
import capitulo06.bookstore.model.NF;

public class NFEmissor {

	private SubmissionPublisher<NF> publisher;

	public NFEmissor() {
		this.publisher = new SubmissionPublisher<>();
		publisher.subscribe(new NFSubscriber());
	}

	public void emit(String clientName, Book book) {
		NF nf = new NF(clientName, book.getName(), 39.99);
		publisher.submit(nf);
	}

	public void close() {
		this.publisher.close();
	}
}
