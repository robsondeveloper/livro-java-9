package capitulo06.bookstore.service;

import capitulo06.bookstore.model.NF;

public class WSPrefeitura {

	public static void emit(NF nf) {
		try {
			System.out.println("emitindo...");
			Thread.sleep(5000);
			System.out.println("emitido!");
		} catch (Exception e) {
			System.out.println("falha ao emitir a nf");
		}
	}
}
