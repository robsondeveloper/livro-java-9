package capitulo04;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;

public class AsyncRequest {

	public static void main(String[] args) throws Exception {

		HttpClient.newHttpClient()
				.sendAsync(HttpRequest.newBuilder().uri(new URI("https://turini.github.io/livro-java-9/books.csv"))
						.GET().build(), HttpResponse.BodyHandlers.ofString())
				.whenComplete((r, t) -> System.out.println(r.body()));

		System.out.println("Aperte o enter para sair");
		new Scanner(System.in).nextLine();
	}
}
