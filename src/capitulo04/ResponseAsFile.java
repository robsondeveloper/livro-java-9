package capitulo04;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Paths;
import java.util.Scanner;

public class ResponseAsFile {

	public static void main(String[] args) throws Exception {

		HttpClient.newHttpClient()
				.sendAsync(HttpRequest.newBuilder().uri(new URI("https://turini.github.io/livro-java-9/books.csv"))
						.GET().build(), HttpResponse.BodyHandlers.ofFile(Paths.get("books.csv")))
				.whenComplete((r, t) -> System.out.println("arquivo salvo em: " + r.body().toAbsolutePath()));

		System.out.println("Aperte o enter para sair");
		new Scanner(System.in).nextLine();
	}
}
